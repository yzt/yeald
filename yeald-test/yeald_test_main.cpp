#include "../yeald/common.h"
#include "../yeald/lexer.h"
#include "../yeald/parser.h"

#include <fstream>
#include <iostream>

using namespace std;

void test_input_stream (string file_name)
{
	ifstream fin (file_name, ios::binary);
	//ifstream fin ("test.txt", ios::binary);
	yeald::StdInputStream<std::ifstream> ins (file_name, fin);

	while (!ins.eoi())
	{
		cout << ins.curr();
		ins.pop ();
	}

	cout
		<< ins.location().file << ": "
		<< ins.location().line_no << ", "
		<< ins.location().column_no << " ("
		<< ins.location().char_no << ")"
		<< endl;
}

void test_lexer (string file_name)
{
	ifstream fin (file_name, ios::binary);
	yeald::StdInputStream<std::ifstream> ins (file_name, fin);
	yeald::Lexer lexer (ins);

	while (!lexer.eoi())
	{
		auto const & t = lexer.curr();
		cout << yeald::Token::Name(t.type()) << "(" << int(t.type()) << "), |" << t.valueUncooked() << "|";
		if (t.type() == yeald::TT::IntLiteral) cout << "(" << t.valueInt() << ")";
		if (t.type() == yeald::TT::RealLiteral) cout << "[" << t.valueReal() << "]";
		if (t.type() == yeald::TT::BoolLiteral) cout << "{" << t.valueBool() << "}";
		if (t.type() == yeald::TT::StringLiteral) cout << "<" << t.valueStr() << ">";
		cout << "  " << "(" << t.location().line_no << "," << t.location().column_no << ")" << endl;

		lexer.pop ();
	}

	cout << "\n";
	cout << "There were " << lexer.errors().size() << " error(s)." << "\n";
	for (auto const & e : lexer.errors())
		cout << e.what() << "\n";
}

void test_parser (string file_name)
{
	ifstream fin (file_name, ios::binary);
	yeald::StdInputStream<std::ifstream> ins (file_name, fin);
	yeald::Lexer lexer (ins);
	yeald::Parser parser (lexer);

	auto program = parser.parseProgram ();

	delete program;
}

int main (int argc, char * argv [])
{
	string file_name = "syntax_test.yeald";
	string sep (64, '-');

	test_input_stream (file_name);
	cout << sep << "\n";

	test_lexer (file_name);
	cout << sep << "\n";

	test_parser (file_name);
	cout << sep << "\n";

	return 0;
}
