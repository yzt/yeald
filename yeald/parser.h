#pragma once

//======================================================================

#include "common.h"
#include "error.h"
#include "lexer.h"
#include "ast.h"

#include <vector>

//======================================================================
/*
Operators
---------
?:
||
^^
&&
|
^
&
==	!=
<	<=	>	>=
<<	>>
+	-
*	/	%
+	-	!	~	(all unary)
[]
()
*/

/*
Grammar
-------
program		:= { package } $
package		:= "package" IDENT "{" pkg_body "}"
pkg_body	:= { function }
function	:= "function" IDENT "(" arg_list ")" "->" IDENT "=" func_body ";"
arg_list	:= [arg {"," arg}]
arg			:= type IDENT
type		:= "bool" | "int" | "real" | "string"
func_body	:= expr
expr		:= ter_cond
ter_expr	:= cond "?" expr ":" expr | logic_expr
logic_expr	:= expr

cond		:=

*/
//======================================================================

namespace yeald {

//======================================================================

class Parser
{
private:
	static String const VarTypeStrs [VarTypeCount];

public:
	Parser (Lexer & lexer);

	bool error () const;
	std::vector<Error> const & errors () const;

	AST::Program * parseProgram ();

private:
	AST::Package * parsePackage ();
	AST::PackageBody * parsePackageBody ();
	AST::Function * parseFunction ();
	AST::ArgumentList * parseArgumentList ();
	AST::FunctionBody * parseFunctionBody ();

private:
	void newError (String msg);
	void newError (Token const & tok, String msg);

	bool expect (TT tok, Token & out_tok);
	bool expect (TT tok);
	bool expectKeyword (Keyword kw);
	bool check (TT tok) const;	// Does not pop() the current token

	VarType expectVariableType ();

private:
	Lexer & m_lexer;
	std::vector<Error> m_errors;
};

//----------------------------------------------------------------------
//======================================================================

}	// namespace yeald

//======================================================================
