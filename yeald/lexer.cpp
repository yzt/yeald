//======================================================================

#include "lexer.h"

#include <limits>

//======================================================================

namespace yeald {

//======================================================================

Lexer::Lexer (InputStream & input)
	: m_input (input)
	, m_cur_tok ()
	, m_errors ()
{
	pop ();
}

//----------------------------------------------------------------------

bool Lexer::pop ()
{
	if (m_input.error() || m_input.eoi())
	{
		m_cur_tok = Token {TT::EOI, m_input.location()};
		return false;
	}

	auto c = m_input.curr();
	auto loc = m_input.location();

	if (IsWhitespace(c))
	{
		stringToken (TT::Whitespace, IsWhitespace);
		return pop ();	// Ignore whitespace
	}
	
	if (IsIdentStarter(c))
	{
		stringToken (TT::Ident, IsIdentContinuer);

		for (int i = 0; i < KeywordCount; ++i)
			if (Token::KeywordStr(i) == m_cur_tok.valueUncooked())
			{
				m_cur_tok.convertIdentToKeyword (Keyword(i));
				break;
			}		
	}
	else if (IsDigit(c))
	{
		String int_part = m_input.readWhile (IsDigit);
		c = m_input.curr ();
		if ('.' == c)	// Real value with a dot
		{
			m_input.pop ();
			String frac_part = m_input.readWhile (IsDigit);
			m_cur_tok = Token {
				TT::RealLiteral, loc,
				int_part + '.' + frac_part,
				parseReal(int_part, frac_part)
			};
		}
		else	// Just an int
		{
			m_cur_tok = Token {TT::IntLiteral, loc, int_part, parseInt(int_part)};
		}
	}
	else if ('"' == c)
	{
		String uncooked (1, c);
		String cooked;
		bool ok = false;
		bool nok = false;

		while (!nok && m_input.pop())
		{
			c = m_input.curr();
			uncooked += c;
			if ('\"' == c)
			{
				m_input.pop();
				ok = true;
				break;
			}
			else if ('\\' == c)
			{
				m_input.pop ();
				c = m_input.curr ();
				uncooked += c;
				switch (c)
				{
					case 'n': cooked += '\n'; break;
					case 'r': cooked += '\r'; break;
					case 'f': cooked += '\f'; break;
					case 'v': cooked += '\v'; break;
					case 't': cooked += '\t'; break;
					case 'b': cooked += '\b'; break;
					case 'a': cooked += '\a'; break;
					case '\\': cooked += '\\'; break;
					case '\"': cooked += '\"'; break;
					//?//case '\'': cooked += '\''; break;
					//case '\x': break;
					//case '\u': break;
					default:
						newError("Error in escaped sequence.");
						nok = true;
						break;
				}
			}
			else
				cooked += c;
		}

		if (ok)
			m_cur_tok = Token {TT::StringLiteral, loc, uncooked, cooked};
		else
			m_cur_tok = Token {TT::Error, loc};
	}
	else if ('#' == c)
	{
		stringToken (TT::Comment, [](Char c){return !IsNewline(c);});
		return pop();	// Ignore comment
	}
	else if ('{' == c)	singleCharToken (TT::CBraceOpn);
	else if ('}' == c)	singleCharToken (TT::CBraceCls);
	else if ('(' == c)	singleCharToken (TT::ParenOpn);
	else if (')' == c)	singleCharToken (TT::ParenCls);
	else if (';' == c)	singleCharToken (TT::Semicolon);
	else if (',' == c)	singleCharToken (TT::Comma);
	else if ('=' == c)
	{
		singleCharToken (TT::Assignment);
		if ('=' == m_input.curr()) appendAChar (TT::Equal);
	}
	else if ('!' == c)
	{
		singleCharToken (TT::LogicalNot);
		if ('=' == m_input.curr()) appendAChar (TT::NotEqual);
	}
	else if ('<' == c)
	{
		singleCharToken (TT::Less);
		if ('=' == m_input.curr()) appendAChar (TT::LessEq);
		else if ('?' == m_input.curr()) appendAChar (TT::Min);
	}
	else if ('>' == c)
	{
		singleCharToken (TT::Greater);
		if ('=' == m_input.curr()) appendAChar (TT::GreaterEq);
		else if ('?' == m_input.curr()) appendAChar (TT::Max);
	}
	else if ('&' == c)
	{
		singleCharToken (TT::BitwiseAnd);
		if ('&' == m_input.curr()) appendAChar (TT::LogicalAnd);
	}
	else if ('|' == c)
	{
		singleCharToken (TT::BitwiseOr);
		if ('|' == m_input.curr()) appendAChar (TT::LogicalOr);
	}
	else if ('^' == c)
	{
		singleCharToken (TT::BitwiseXor);
		if ('^' == m_input.curr()) appendAChar (TT::LogicalXor);
	}
	else if ('~' == c)	singleCharToken (TT::BitwiseNot);
	else if ('?' == c)	singleCharToken (TT::QuestionMark);
	else if (':' == c)	singleCharToken (TT::Colon);
	else if ('+' == c)	singleCharToken (TT::Plus);
	else if ('-' == c)
	{
		singleCharToken (TT::Minus);
		if ('>' == m_input.curr()) appendAChar (TT::ArrowRght);
	}
	else if ('*' == c)
	{
		singleCharToken (TT::Asterisk);
		if ('*' == m_input.curr()) appendAChar (TT::Power);
	}
	else if ('/' == c)	singleCharToken (TT::Slash);
	else
		singleCharToken (TT::Error);

	return !m_input.eoi() && !m_input.error();
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

void Lexer::newError (String msg)
{
	m_errors.emplace_back (Error::Lexer(m_input.location(), std::move(msg)));
}

//----------------------------------------------------------------------

void Lexer::singleCharToken (TT type)
{
	m_cur_tok = Token {type, m_input.location(), {m_input.curr()}};
	m_input.pop ();
}

//----------------------------------------------------------------------

void Lexer::convertIdentToKeyword (Keyword kw)
{
	assert (TT::Ident == m_cur_tok.type());
	m_cur_tok = Token {TT::Keyword, m_cur_tok.location(), m_cur_tok.valueUncooked(), kw};
}

//----------------------------------------------------------------------

void Lexer::appendAChar (TT new_type)
{
	m_cur_tok = Token {new_type, m_cur_tok.location(), m_cur_tok.valueUncooked() + m_input.curr()};
	m_input.pop ();
}

//----------------------------------------------------------------------

Int Lexer::parseInt (String const & s)
{
	Int ret = 0;
	for (auto c : s)
	{
		int d = (c - '0');
		if (ret > ((std::numeric_limits<Int>::max() - d) / 10))
		{
			newError ("Integer is out of bounds.");
			break;
		}
		ret = ret * 10 + d;
	}

	return ret;
}

//----------------------------------------------------------------------

Real Lexer::parseReal (String const & int_part, String const & frac_part)
{
	Real r = 0;
	for (auto c : int_part)
		r = r * 10 + (c - '0');
	Real f = 0;
	for (auto i = frac_part.rbegin(), e = frac_part.rend(); i != e; ++i)
		f = f / 10 + (*i - '0');
	return r + (f / 10);
}

//----------------------------------------------------------------------

Bool Lexer::parseBool (String const & s)
{
	if (s == "true") return true;
	else if (s == "false") return false;

	newError ("Bool literal is invalid.");
	return false;
}

//----------------------------------------------------------------------

//String Lexer::parseString (String const & s)
//{
//	return "";
//}

//----------------------------------------------------------------------
//======================================================================

}	// namespace yeald

//======================================================================
