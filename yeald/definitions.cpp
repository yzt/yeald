//======================================================================

#include "definitions.h"

//======================================================================

namespace yeald {

//======================================================================

#define TT_STRS(v, e)	STRINGIZE(e),
String const Token::ms_token_str [TTCount] = { YEALD_PRIV__TOKEN_TYPES(TT_STRS) };
#undef TT_STRS

#define KW_STRS(v, e, s)	s,
String const Token::ms_keyword_str [KeywordCount] = { YEALD_PRIV__KEYWORDS(KW_STRS) };
#undef KW_STRS

//----------------------------------------------------------------------

String const & Token::Name (TT tt)
{
	return ms_token_str[int(tt)];
}

//----------------------------------------------------------------------

String const & Token::KeywordStr (int kw)
{
	return ms_keyword_str[kw];
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

Token::Token (TT type, Location const & location, String uncooked_value)
	: m_type (type)
	, m_location (location)
	, m_uncooked_value (std::move(uncooked_value))
{
}

//----------------------------------------------------------------------

Token::Token (TT type, Location const & location, String uncooked_value, Bool bool_value)
	: m_type (type)
	, m_location (location)
	, m_uncooked_value (std::move(uncooked_value))
{
	m_value.b = bool_value;
}

//----------------------------------------------------------------------

Token::Token (TT type, Location const & location, String uncooked_value, Int int_value)
	: m_type (type)
	, m_location (location)
	, m_uncooked_value (std::move(uncooked_value))
{
	m_value.i = int_value;
}

//----------------------------------------------------------------------

Token::Token (TT type, Location const & location, String uncooked_value, Keyword kw_value)
	: m_type (type)
	, m_location (location)
	, m_uncooked_value (std::move(uncooked_value))
{
	m_value.k = kw_value;
}

//----------------------------------------------------------------------

Token::Token (TT type, Location const & location, String uncooked_value, Real real_value)
	: m_type (type)
	, m_location (location)
	, m_uncooked_value (std::move(uncooked_value))
{
	m_value.r = real_value;
}

//----------------------------------------------------------------------

Token::Token (TT type, Location const & location, String uncooked_value, String string_value)
	: m_type (type)
	, m_location (location)
	, m_uncooked_value (std::move(uncooked_value))
{
	m_value.s = std::move(string_value);
}

//----------------------------------------------------------------------

void Token::convertIdentToKeyword (Keyword kw)
{
	assert (TT::Ident == m_type);
	m_type = TT::Keyword;
	m_value.k = kw;
}

//======================================================================
//----------------------------------------------------------------------
//======================================================================

}	// namespace yeald

//======================================================================
