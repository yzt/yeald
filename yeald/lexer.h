#pragma once

//======================================================================

#include "common.h"
#include "definitions.h"
#include "error.h"

//======================================================================

namespace yeald {

//======================================================================

class Lexer
{
public:
	explicit Lexer (InputStream & input);

	bool eoi () const {return m_cur_tok.type() == TT::EOI;}
	bool error () const {return !m_errors.empty();}
	std::vector<Error> const & errors () const {return m_errors;}

	bool pop ();
	Token const & curr () const {return m_cur_tok;}

private:
	void newError (String msg);

	void singleCharToken (TT type);
	void convertIdentToKeyword (Keyword kw);
	void appendAChar (TT new_type);
	
	template <typename F>
	void stringToken (TT type, F const & f)
	{
		auto loc = m_input.location();
		m_cur_tok = Token {type, loc, m_input.readWhile(f)};
	}

	Int parseInt (String const & s);
	Real parseReal (String const & int_part, String const & frac_part);
	Bool parseBool (String const & s);
//	String parseString (String const & s);

private:
	InputStream & m_input;
	Token m_cur_tok;
	std::vector<Error> m_errors;
};

//----------------------------------------------------------------------
//======================================================================

}	// namespace yeald

//======================================================================
