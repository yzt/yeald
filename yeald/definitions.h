#pragma once

//======================================================================

#include "common.h"

//======================================================================

#define STRINGIZE_IMPL(s)	#s
#define STRINGIZE(s)		STRINGIZE_IMPL(s)

//======================================================================

#define YEALD_PRIV__TOKEN_TYPES(action)								\
	action ( 0, Empty)			/**/								\
	action ( 1, Error)												\
	action ( 2, EOI)												\
	action ( 3, Whitespace)		/*   */								\
	action ( 4, Comment)		/* # */								\
	action ( 5, Keyword)		/* function */						\
	action ( 6, Ident)			/* foo */							\
	action ( 7, IntLiteral)		/* 42 */							\
	action ( 8, BoolLiteral)	/* true */							\
	action ( 9, RealLiteral)	/* 4.2 */							\
	action (10, StringLiteral)	/* "Hello" */						\
	action (11, CBraceOpn)		/* { */								\
	action (12, CBraceCls)		/* } */								\
	action (13, ParenOpn)		/* ( */								\
	action (14, ParenCls)		/* ) */								\
	action (15, Semicolon)		/* ; */								\
	action (16, Comma)			/* , */								\
	action (17, ArrowRght)		/* -> */							\
	action (18, Assignment)		/* = */								\
	action (19, QuestionMark)	/* ? */								\
	action (20, Colon)			/* : */								\
	action (21, Plus)			/* + */								\
	action (22, Minus)			/* - */								\
	action (23, Asterisk)		/* * */								\
	action (24, Slash)			/* / */								\
	action (25, Power)			/* ** */							\
	action (26, Equal)			/* == */							\
	action (27, NotEqual)		/* != */							\
	action (28, Less)			/* < */								\
	action (29, LessEq)			/* <= */							\
	action (30, Greater)		/* > */								\
	action (31, GreaterEq)		/* >= */							\
	action (32, Max)			/* >? */							\
	action (33, Min)			/* <? */							\
	action (34, BitwiseAnd)		/* & */								\
	action (35, BitwiseOr)		/* | */								\
	action (36, BitwiseXor)		/* ^ */								\
	action (37, BitwiseNot)		/* ~ */								\
	action (38, LogicalAnd)		/* && */							\
	action (39, LogicalOr)		/* || */							\
	action (40, LogicalXor)		/* ^^ */							\
	action (41, LogicalNot)		/* ! */

//======================================================================

#define YEALD_PRIV__KEYWORDS(action)								\
	action ( 0, Package,	"package")								\
	action ( 1, Function,	"function")								\
	action ( 2, Bool,		"bool")									\
	action ( 3, Int,		"int")									\
	action ( 4, Real,		"real")									\
	action ( 5, String,		"string")								\
	action ( 6, False,		"false")								\
	action ( 7, True,		"true")

//======================================================================

#define YEALD_PRIV__VARIABLE_TYPES(action)							\
	action ( 0, Invalid,	"")										\
	action ( 1, Bool,		"bool")									\
	action ( 2, Int,		"int")									\
	action ( 3, Real,		"real")									\
	action ( 4, String,		"string")								

//======================================================================

#define YEALD_PRIV__AST_NODES(action)								\
	action ( 0, Invalid)											\
	action ( 1, Program)											\
	action ( 2, Package)											\
	action ( 3, PkgBody)											\
	action ( 4, Function)											\
	action ( 5, ArgList)											\
	action ( 6, Arg)												\
	action ( 7, TypeName)											\
	action ( 8, FuncBody)												

//======================================================================

namespace yeald {

//======================================================================

#define KW_ENUM(v, e, s)	e = v,
enum class Keyword { YEALD_PRIV__KEYWORDS(KW_ENUM) };
#undef KW_ENUM

#define KW_COUNT(v, e, s)	+1
static int const KeywordCount = YEALD_PRIV__KEYWORDS(KW_COUNT);
#undef KW_COUNT

//----------------------------------------------------------------------

#define TT_ENUM(v, e)	e = v,
enum class TT { YEALD_PRIV__TOKEN_TYPES(TT_ENUM) };
#undef TT_ENUM

#define TT_COUNT(v, e) +1
static int const TTCount = YEALD_PRIV__TOKEN_TYPES(TT_COUNT);
#undef TT_COUNT

//----------------------------------------------------------------------

#define VARIABLETYPE_ENUM(v, e, s)	e = v,
enum class VarType { YEALD_PRIV__VARIABLE_TYPES(VARIABLETYPE_ENUM) };
#undef VARIABLETYPE_ENUM

#define VARIABLETYPE_COUNT(v, e, s)	+1
static int const VarTypeCount = YEALD_PRIV__VARIABLE_TYPES(VARIABLETYPE_COUNT);
#undef VARIABLETYPE_ENUM

//======================================================================

class Token
{
private:
	static String const ms_token_str [TTCount];
	static String const ms_keyword_str [KeywordCount];

public:
	static String const & Name (TT tt);
	static String const & KeywordStr (int kw);
	static String const & KeywordStr (Keyword kw) {return KeywordStr(int(kw));}
	
public:
	Token () : m_type (TT::Empty), m_location (nullptr) {}
	Token (TT type, Location const & location) : m_type (type), m_location (location) {}
	Token (TT type, Location const & location, String uncooked_value);
	Token (TT type, Location const & location, String uncooked_value, Bool bool_value);
	Token (TT type, Location const & location, String uncooked_value, Int int_value);
	Token (TT type, Location const & location, String uncooked_value, Keyword kw_value);
	Token (TT type, Location const & location, String uncooked_value, Real real_value);
	Token (TT type, Location const & location, String uncooked_value, String string_value);

	explicit operator bool () const {return m_type != TT::Empty;}

	TT type () const {return m_type;}
	String const & typeName () const {return Name(m_type);}
	Location const & location () const {return m_location;}
	String const & valueUncooked () const {return m_uncooked_value;}
	Bool valueBool () const {return m_value.b;}
	Int valueInt () const {return m_value.i;}
	Keyword valueKeyword () const {return m_value.k;}
	Real valueReal () const {return m_value.r;}
	String const & valueStr () const {return m_value.s;}

	void convertIdentToKeyword (Keyword kw);

private:
	TT m_type;
	Location m_location;
	String m_uncooked_value;
	struct {
		String s;
		Real r;
		Int i;
		Keyword k;
		Bool b;
	} m_value;
};

//======================================================================
//----------------------------------------------------------------------
//======================================================================

}	// namespace yeald

//======================================================================
