#pragma once

//======================================================================
// Everybody needs these:
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <string>
#include <utility>
#include <vector>

//======================================================================

#if defined(_MSC_VER)
	#define Y_NOEXCEPT throw()
#else
	#define Y_NOEXCEPT nothrow
#endif

//======================================================================

namespace yeald {

//======================================================================

typedef char Char;
typedef std::string String;
typedef int64_t Int;
typedef double Real;
typedef bool Bool;

//======================================================================

Char InvalidChar ();
bool IsInvalid (Char c);
bool IsNewline (Char c);
bool IsWhitespace (Char c);
bool IsDigit (Char c);
bool IsLetter (Char c);
bool IsLower (Char c);
bool IsUpper (Char c);
bool IsAlphanum (Char c);
bool IsIdentStarter (Char c);
bool IsIdentContinuer (Char c);

//======================================================================

struct Location
{
	char const * file;
	int line_no, column_no, char_no;

	Location (char const * file_, int line_no_ = 1, int column_no_ = 0, int char_no_ = 0)
		: file (file_), line_no (line_no_), column_no (column_no_), char_no (char_no_)
	{}

	void feed (Char c)
	{
		char_no += 1;
		if (IsNewline(c))
			line_no += 1, column_no = 1;
		else
			column_no += 1;
	}
};

//======================================================================

class InputStream
{
public:
	explicit InputStream (String name)
		: m_name (std::move(name))
		, m_cur_loc (m_name.c_str())
		, m_eoi (false)
		, m_error (false)
		, m_cur_char (InvalidChar())
	{
	}
	virtual ~InputStream () {}

	String const & name () const {return m_name;}
	Location const & location () const {return m_cur_loc;}
	bool error () const {return m_error;}
	bool eoi () const {return m_eoi;}

	bool pop ()
	{
		m_cur_char = readOne ();
		if (!eoi() && !error())
			m_cur_loc.feed (m_cur_char);
		return !error() && !eoi();
	}

	Char curr () const {return m_cur_char;}

	// Read and pop and return while f(c) returns true
	template <typename F>
	String readWhile (F const & f)
	{
		String ret;

		while (f(curr()))
		{
			ret += curr();
			if (!pop())
				break;
		}

		return ret;
	}

protected:
	void setEOI () {m_eoi = true;}
	void resetEOI () {m_eoi = false;}
	void setError () {m_error = true;}
	void resetError () {m_error = false;}

	virtual Char readOne () = 0;

private:
	String const m_name;
	Location m_cur_loc;
	bool m_eoi;
	bool m_error;
	Char m_cur_char;
};

//======================================================================

template <typename Stream>	// as in ifstream or stringstream
class StdInputStream
	: public InputStream
{
public:
	explicit StdInputStream (String name, Stream & stream)
		: InputStream (std::move(name))
		, m_stream (stream)
	{
		pop ();
	}

protected:
	Char readOne () override
	{
		auto c = m_stream.get();
		if (Stream::traits_type::eof() == c)
		{
			setEOI ();
			return InvalidChar();
		}
		else
			return Char(c);
	}

private:
	Stream & m_stream;
};

//======================================================================
//----------------------------------------------------------------------
//======================================================================

}	// namespace yeald

//======================================================================
