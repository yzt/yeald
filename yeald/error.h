#pragma once

//======================================================================

#include "common.h"
#include <exception>

//======================================================================

namespace yeald {

//======================================================================

class Error
	: public std::exception
{
public:
	enum class Category {
		Unknown,
		Lexer,
		Parser,
		CodeGen,
		VM,
	};

public:
	static Error Lexer (Location const & loc, String msg = "") {return Error{Category::Lexer, loc, std::move(msg)};}
	static Error Parser (Location const & loc, String msg = "") {return Error{Category::Parser, loc, std::move(msg)};}
	static Error CodeGen (Location const & loc, String msg = "") {return Error{Category::CodeGen, loc, std::move(msg)};}
	static Error VM (Location const & loc, String msg = "") {return Error{Category::VM, loc, std::move(msg)};}
	static Error Other (Location const & loc, String msg = "") {return Error{Category::Unknown, loc, std::move(msg)};}

public:
	Error (Category category, Location const & location, String message);

	char const * what () const Y_NOEXCEPT override;

private:
	Category m_category;
	String m_what;
};

//======================================================================
//----------------------------------------------------------------------
//======================================================================

}

//======================================================================
