#pragma once

//======================================================================

#include "common.h"
#include "definitions.h"

//======================================================================

namespace yeald {
	namespace AST {

//======================================================================

#define NODETYPE_ENUM(v, e)		e = v,
enum class Type { YEALD_PRIV__AST_NODES(NODETYPE_ENUM) };
#undef NODETYPE_ENUM

//======================================================================

class Node;
typedef Node * NodePtr;
typedef Node const * NodeConstPtr;

//----------------------------------------------------------------------

class Node
{
private:
	#define NODETYPE_COUNT(v, e) +1
	static const int TypeCount = YEALD_PRIV__AST_NODES(NODETYPE_COUNT);
	#undef NODETYPE_COUNT

	static const String TypeStrs [TypeCount];
public:
	static String const & TypeStr (Type type) {return TypeStrs[unsigned(type)];}

protected:
	explicit Node (Type type) : m_type (type) {}

public:
	virtual ~Node () {}
	
	Type type () const {return m_type;}
	String const & typeStr () const {return TypeStr(type());}

protected:
	Type m_type;
};

//----------------------------------------------------------------------

class NodeWithChildren : public Node
{
public:
	typedef std::vector<NodePtr> ChildContainer;

protected:
	explicit NodeWithChildren (Type type) : Node (type) {}

public:
	virtual ~NodeWithChildren ();
	
	unsigned childCount () const {return unsigned(m_children.size());}
	ChildContainer & children () {return m_children;}
	ChildContainer const & children () const {return m_children;}
	NodePtr child (unsigned idx) {return (idx < childCount()) ? m_children[idx] : nullptr;}
	NodeConstPtr child (unsigned idx) const {return (idx < childCount()) ? m_children[idx] : nullptr;}

	void addChild (NodePtr child) {m_children.push_back (child);}

protected:
	ChildContainer m_children;
};

//======================================================================

class Program : public NodeWithChildren
{
public:
	Program () : NodeWithChildren (Type::Program) {}
};

//======================================================================

class Package : public Node
{
public:
	Package (String name_, NodePtr body_)
		: Node (Type::Package)
		, name (std::move(name_))
		, body (body_)
	{}
	~Package () {delete body;}

protected:
	String name;
	NodePtr body;
};

//======================================================================

class PackageBody : public NodeWithChildren
{
public:
	PackageBody () : NodeWithChildren (Type::PkgBody) {}
};

//======================================================================

class FunctionBody : public Node {};
class ArgumentList : public NodeWithChildren {};

//======================================================================

class Function : public Node
{
public:
	Function (String name_, VarType return_type_, NodePtr arg_list_, NodePtr body_)
		: Node (Type::Function)
		, name (std::move(name_))
		, return_type (return_type_)
		, arg_list (arg_list_)
		, body (body_)
	{}

	~Function ()
	{
		delete body;
		delete arg_list;
	}

protected:
	String name;
	VarType return_type;
	NodePtr arg_list;
	NodePtr body;
};

//======================================================================

	}	// namespace AST
}	// namespace yeald

//======================================================================
