//======================================================================

#include "parser.h"

//======================================================================

namespace yeald {

//======================================================================

#define VARIABLETYPE_STRS(v, e, s)	s,
String const Parser::VarTypeStrs [VarTypeCount] = { YEALD_PRIV__VARIABLE_TYPES(VARIABLETYPE_STRS) };
#undef VARIABLETYPE_STRS

//----------------------------------------------------------------------
//----------------------------------------------------------------------

Parser::Parser (Lexer & lexer)
	: m_lexer (lexer)
{
}

//----------------------------------------------------------------------

bool Parser::error () const
{
	return !m_errors.empty();
}

//----------------------------------------------------------------------

std::vector<Error> const & Parser::errors () const
{
	return m_errors;
}

//----------------------------------------------------------------------

AST::Program * Parser::parseProgram ()
{
	AST::Program * ret = new AST::Program;

	while (auto pkg = parsePackage())
		ret->addChild (pkg);

	expect (TT::EOI);

	return ret;
}

//----------------------------------------------------------------------

AST::Package * Parser::parsePackage ()
{
	AST::Package * ret = nullptr;

	expectKeyword (Keyword::Package);
	Token name;
	expect (TT::Ident, name);
	expect (TT::CBraceOpn);
	auto body = parsePackageBody();
	expect (TT::CBraceCls);

	ret = new AST::Package (name.valueStr(), body);

	return ret;
}

//----------------------------------------------------------------------

AST::PackageBody * Parser::parsePackageBody ()
{
	AST::PackageBody * ret = new AST::PackageBody;

	while (auto func = parseFunction())
		ret->addChild (func);

	return ret;
}

//----------------------------------------------------------------------

AST::Function * Parser::parseFunction ()
{
	AST::Function * ret = nullptr;

	expectKeyword (Keyword::Function);
	Token name;
	expect (TT::Ident, name);
	expect (TT::ParenOpn);
	auto arg_list = parseArgumentList ();
	expect (TT::ParenCls);
	expect (TT::ArrowRght);
	auto ret_type = expectVariableType ();
	expect (TT::Assignment);
	auto body = parseFunctionBody ();
	expect (TT::Semicolon);

	ret = new AST::Function (name.valueStr(), ret_type, arg_list, body);

	return ret;
}

//----------------------------------------------------------------------

AST::ArgumentList * Parser::parseArgumentList ()
{
	return nullptr;
}

//----------------------------------------------------------------------

AST::FunctionBody * Parser::parseFunctionBody ()
{
	return nullptr;
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

void Parser::newError (String msg)
{
	m_errors.emplace_back (Error::Category::Parser, m_lexer.curr().location(), std::move(msg));
}

//----------------------------------------------------------------------

void Parser::newError (Token const & tok, String msg)
{
	m_errors.emplace_back (Error::Category::Parser, tok.location(), std::move(msg));
}

//----------------------------------------------------------------------

bool Parser::expect (TT tok, Token & out_tok)
{
	if (m_lexer.curr().type() == tok)
	{
		out_tok = m_lexer.curr();
		m_lexer.pop();
		return true;
	}
	else
	{
		newError ("Expected \"" + Token::Name(tok) + "\", but found \"" + m_lexer.curr().typeName() + "\" instead.");
		return false;
	}
}

//----------------------------------------------------------------------

bool Parser::expect (TT tok)
{
	if (m_lexer.curr().type() == tok)
	{
		m_lexer.pop();
		return true;
	}
	else
	{
		newError ("Expected \"" + Token::Name(tok) + "\", but found \"" + m_lexer.curr().typeName() + "\" instead.");
		return false;
	}
}

//----------------------------------------------------------------------

bool Parser::expectKeyword (Keyword kw)
{
	if (m_lexer.curr().type() == TT::Keyword && m_lexer.curr().valueKeyword() == kw)
	{
		m_lexer.pop();
		return true;
	}
	else
	{
		newError ("Expected keyword \"" + Token::KeywordStr(kw) + "\".");
		return false;
	}
}

//----------------------------------------------------------------------

bool Parser::check (TT tok) const
{
	return m_lexer.curr().type() == tok;
}

//----------------------------------------------------------------------

VarType Parser::expectVariableType ()
{
	VarType ret = VarType::Invalid;

	if (m_lexer.curr().type() == TT::Keyword)
	{
		for (int i = 0; i < VarTypeCount; ++i)
			if (VarTypeStrs[i] == m_lexer.curr().valueStr())
			{
				ret = VarType(i);
				m_lexer.pop ();
				break;
			}
	}

	return ret;
}

//======================================================================

}	// namespace yeald

//======================================================================
