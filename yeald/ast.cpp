//======================================================================

#include "ast.h"

//======================================================================

namespace yeald {
	namespace AST {

//======================================================================

#define NODETYPE_STRS(v, e) STRINGIZE(e),
const String Node::TypeStrs [TypeCount] = { YEALD_PRIV__AST_NODES(NODETYPE_STRS) };
#undef NODETYPE_STRS

//======================================================================

NodeWithChildren::~NodeWithChildren ()
{
	for (auto & ch : m_children)
		delete ch;
	m_children.clear ();
}

//----------------------------------------------------------------------
//======================================================================

	}	// namespace AST
}	// namespace yeald

//======================================================================
