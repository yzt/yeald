//======================================================================

#include "error.h"

//======================================================================

namespace yeald {

//======================================================================

Error::Error (Category cat, Location const & location, String message)
	: m_category (cat)
{
	m_what = String(location.file) + "(" + std::to_string(location.line_no) + "," + std::to_string(location.column_no) + "): ";
	
	switch (m_category)
	{
	case Category::Lexer:	m_what += "[Lexer Error]";		break;
	case Category::Parser:	m_what += "[Parser Error]";		break;
	case Category::CodeGen:	m_what += "[CodeGen Error]";	break;
	case Category::VM:		m_what += "[VM Error]";			break;
	default:				m_what += "[Other Error]";		break;
	}

	if (!message.empty())
		m_what += " " + message;
}

//----------------------------------------------------------------------

char const * Error::what () const Y_NOEXCEPT
{
	return m_what.c_str();
}

//----------------------------------------------------------------------
//======================================================================

}

//======================================================================
